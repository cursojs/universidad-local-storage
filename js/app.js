const carrito = document.querySelector('#carrito');
const cursos = document.querySelector('#lista-cursos');
const listaCursos = document.querySelector('#lista-carrito tbody');
const vaciarCarritoBtn = document.querySelector('#vaciar-carrito');

cargarEventListeners();

function cargarEventListeners() {
    cursos.addEventListener('click', comprarCurso);

    carrito.addEventListener('click', eliminarCurso);

    vaciarCarritoBtn.addEventListener('click', vaciarCarrito);

    document.addEventListener('DOMContentLoaded', listaCursosLocalStorage);
}

function comprarCurso(e) {
    e.preventDefault();

    if(e.target.classList.contains('agregar-carrito')) {
        const curso = e.target.parentElement.parentElement;

        leerDatosCurso(curso);
    }
}

function leerDatosCurso(curso) {
    const infoCurso = {
        imagen: curso.querySelector('img').src,
        titulo: curso.querySelector('h4').textContent,
        precio: curso.querySelector('.precio span').textContent,
        id: curso.querySelector('a').getAttribute('data-id')
    };

    insertarCurso(infoCurso);
}

function insertarCurso(curso) {
    const row = document.createElement('tr');
    row.innerHTML = `
        <td>
            <img src="${curso.imagen}" width="100">
        </td>
        <td>${curso.titulo}</td>
        <td>${curso.precio}</td>
        <td>
            <a href="#" class="borrar-curso" data-id="${curso.id}">X</a>
        </td>
    `;
    listaCursos.appendChild(row);

    guardarCursoLocalStorage(curso);
}

function eliminarCurso(e) {
    e.preventDefault();

    if(e.target.classList.contains('borrar-curso')) {
        const curso = e.target.parentElement.parentElement;
        curso.remove();
        const cursoId = curso.querySelector('a').getAttribute('data-id');
        eliminarCursoLocalStorage(cursoId);
    }
}

function vaciarCarrito() {
    while(listaCursos.firstChild) {
        listaCursos.removeChild(listaCursos.firstChild);
    }
    return false;
}

function guardarCursoLocalStorage(curso) {
    let cursos = obtenerCursoLocalStorage();
    cursos = [...cursos, curso];
    localStorage.setItem('cursos', JSON.stringify(cursos));
}

function obtenerCursoLocalStorage() {
    const getCursos = localStorage.getItem('cursos');
    return getCursos === null ? [] : JSON.parse(getCursos);
}

function listaCursosLocalStorage() {
    const getCursos = obtenerCursoLocalStorage();
    getCursos.forEach(curso => {
        const row = document.createElement('tr');
        row.innerHTML = `
            <td>
                <img src="${curso.imagen}" width="100">
            </td>
            <td>${curso.titulo}</td>
            <td>${curso.precio}</td>
            <td>
                <a href="#" class="borrar-curso" data-id="${curso.id}">X</a>
            </td>
        `;
        listaCursos.appendChild(row);
    });
}

function eliminarCursoLocalStorage(cursoId) {
    console.log(cursoId);
    const getCursos = obtenerCursoLocalStorage();

    const cursos = getCursos.filter(it => it.id !== cursoId);
    localStorage.setItem('cursos', JSON.stringify(cursos));
}
